package edu.byui.cit.expcalc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    TextView Result;
    EditText Number, Power;
    Button enter, clear;

    double resultNum;

    double num, pow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Result = (TextView)findViewById(R.id.Result);

        Number = (EditText)findViewById(R.id.Number);
        Power = (EditText)findViewById(R.id.Power);

        enter = (Button)findViewById(R.id.enter);
        clear = (Button)findViewById(R.id.clear);

        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                num = Double.parseDouble(Number.getText().toString());
                pow = Double.parseDouble(Power.getText().toString());

                 resultNum = PowerFormula.power(num,pow);
                 Result.setText(String.valueOf(resultNum));
            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Result.setText(String.valueOf(" "));
                Number.setText(String.valueOf(" "));
                Power.setText(String.valueOf(" "));



            }
        });




    }
}
