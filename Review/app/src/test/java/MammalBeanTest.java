import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class MammalBeanTest {

    @Test
    public void getLegCount() {
    }

    @Test
    public void getcolor() {
    }

    @Test
    public void getHeight() {
    }

    @Test
    public void setLegCount() {
    }

    @Test
    public void setColor() {
    }

    @Test
    public void setHeight() {
    }

    @Test
    public void toStringTest1(){
        //TEST FOR SIMILARITIES
        //DogBean test1 = new  DogBean("Pitbull", "Johnny", 4, "Brown", 6.0);
        MammalBean test1 = new MammalBean('4',"Red",3.0);
        MammalBean test2 = new MammalBean('4',"Red",3.0);

        assertEquals(test1,test2);

    }

    @Test
    public void toStringTest2(){
        //TEST FOR DIFFERENCE
        //DogBean test1 = new  DogBean("Pitbull", "Johnny", 4, "Brown", 6.0);

        MammalBean test3 = new MammalBean('2',"blue",7.0);
        MammalBean test4 = new MammalBean('4',"Red",3.0);

        assertEquals(test3,test4);
    }

    public void setTest(){

        //TESTING SET SIMILAR
        Set setMammal = new HashSet();
        Set setMammal2 = new HashSet();

        MammalBean test1 = new MammalBean('4',"Red",3.0);
        MammalBean test2 = new MammalBean('4',"Red",3.0);
        MammalBean test3 = new MammalBean('2',"blue",7.0);
        MammalBean test4 = new MammalBean('4',"Red",3.0);

        setMammal.add(test1);
        setMammal.add(test2);
        setMammal.add(test3);
        setMammal.add(test4);

        MammalBean test5 = new MammalBean('4',"Red",3.0);
        MammalBean test6 = new MammalBean('4',"Red",3.0);
        MammalBean test7 = new MammalBean('2',"blue",7.0);
        MammalBean test8 = new MammalBean('4',"Red",3.0);

        setMammal2.add(test5);
        setMammal2.add(test6);
        setMammal2.add(test7);
        setMammal2.add(test8);

        assertEquals(setMammal,setMammal2);
    }

}