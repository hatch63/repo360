public class MammalBean {

    //private properties
    private int legCount;
    private String color;
    private double height;

    //Constructor
    public MammalBean(int MlegCount,String Mcolor, double Mheight){
        legCount = MlegCount;
        color = Mcolor;
        height = Mheight;
    }

/////////////////////////////////////////////////////////////////////////////////////////////////

    //Getters
    public int getLegCount(){
        return this.legCount;
    }

    public String getcolor(){
        return this.color;
    }

    public double getHeight(){
        return this.height;
    }

/////////////////////////////////////////////////////////////////////////////////////////////////

    //Setters
    public void setLegCount(int MlegCount){
        this.legCount = MlegCount;

    }

    public void setColor(String Mcolor){
        this.color = Mcolor;
    }

    public void setHeight(double Mheight){
        this.height = Mheight;
    }



    public String toString(){
        return "Leg Count: " + legCount + " Color: " + color + " Height: " + height;
    }

    public void setTest(){

    }

}
