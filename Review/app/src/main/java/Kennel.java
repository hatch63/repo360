
import java.util .Arrays;

public class Kennel {


    public DogBean[] buildDogs(){

        //Build dogs puts all of these dog objects into an array that I called dogArray. Then it returns
        //it so that it can be passed to display dogs.

        DogBean [] dogArray = new DogBean[4];
        dogArray[0] = new DogBean("Pitbull", "Johnny", 4, "Brown", 6.0);
        dogArray[1] = new DogBean("German Shephard", "Ellie", 4, "Black", 5.0);
        dogArray[2] = new DogBean("Corgi", "winston", 4, "Red", 2.0);
        dogArray[3] = new DogBean("BullDog", "Nigel", 4, "White", 3.0);
        dogArray[4] = new DogBean("Husky", "Mr.T", 4, "Grey", 4.0);

        return dogArray;
    }

////////////////////////////////////////////////////////////////////////////////////////////////

    //Display Dogs-Don't know how to pass the array over to this method so that I can display
    //the dog objects.

    public void displayDogs(DogBean[] dogArray){
        for(DogBean DogBean: dogArray ){
            System.out.println(dogArray);
        }
        System.out.println(Arrays.toString(dogArray));

    }



//////////////////////////////////////////////////////////////////////////////////////////////////

    //MAIN
    //Runs all of your code. Builds the dogArray putting in the hard coded objects then runs
    //displaydogs to show the information.
    public static void main(){




    }

}

