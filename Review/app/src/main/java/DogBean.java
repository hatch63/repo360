
public class DogBean extends MammalBean {

    private String breed;
    private String name;

    DogBean(String dBreed, String dName, int MlegCount, String Mcolor, double Mheight ){

        //Super from Mammal Class
        super(MlegCount,Mcolor, Mheight);

        //Constructor for Dogs
        breed = dBreed;
        name = dName;

    }

////////////////////////////////////////////////////////////////////////////////////////////////

//Getters

    public String getBreed(){
        return this.breed;
    }

    public String getname(){
        return this.name;
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

//Setters

    public void setBreed ( String dBreed){
        this.breed = dBreed;
    }

    public void setname(String dName){
        this.name = dName;
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    //To String
    public String toString(){
        return "Breed: " + breed + "Name: " + name + "Leg Count: " + getLegCount() + "Color: " +
                getcolor() + "Height: " + getHeight();
    }

}
