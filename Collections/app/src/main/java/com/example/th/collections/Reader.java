package com.example.th.collections;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Reader
{
    //Delimiters used in the CSV file
    private static final String COMMA_DELIMITER = ",";

    public static void main(String args[])
    {
        BufferedReader br = null;
        try
        {
            //Reading the csv file
            br = new BufferedReader(new FileReader("app/src/main/assets/rawData.csv"));
            //app/src/main/assets

            //Create List for holding Employee objects
            List<Account> accList = new ArrayList<Account>();

            String line = "";
            //Read to skip the header
            br.readLine();
            //Reading from the second line
            while ((line = br.readLine()) != null)
            {
                String[] accountDetails = line.split(COMMA_DELIMITER);

                if(accountDetails.length > 0 )
                {
                    //Save the employee details in Employee object
                    Account acc = new Account (accountDetails[0],
                            accountDetails[1],accountDetails[2],
                            accountDetails[3]);
                    accList.add(acc);
                }
            }

            //Lets print the Employee List
            for(Account a : accList)
            {
                System.out.println(a.getAccountNumber()+"   "+a.getAccountType()+"   "
                        + a.getName() + "   "+a.getFamilyName());
            }
        }
        catch(Exception ee)
        {
            ee.printStackTrace();
        }
        finally
        {
            try
            {
                br.close();
            }
            catch(IOException ie)
            {
                System.out.println("Error occured while closing the BufferedReader");
                ie.printStackTrace();
            }
        }
    }
}
