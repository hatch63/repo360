package com.example.th.collections;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.FileNotFoundException;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Map;


public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        TextView console = findViewById(R.id.console);
        System.setOut(new PrintStream(new TextViewWriter(console)));
    }

    private static final class TextViewWriter extends OutputStream {
        private final StringBuilder buffer;
        private final TextView console;

        TextViewWriter(TextView console) {
            this.buffer = new StringBuilder();
            this.console = console;
        }

        @Override
        public void write(int b) {
            buffer.append(b);
            console.setText(buffer);
        }

        @Override
        public void write(@NotNull byte[] b, int offs, int len) {
            buffer.append(new String(b, offs, len));
            console.setText(buffer);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    //Delimiters used in the CSV file
    private static final String COMMA_DELIMITER = ",";

    private void main() {
        /* Write your code in this function as if this function were
         * public static void main(String[] args)
         */
        System.out.println("Elephants are big.");
        System.out.println("Spaceships are expensive.");


///////////////////////////////////////////////////////////////////////////////////////////////////

        //CODE
        BufferedReader br = null;
        try
        {
            //Reading the csv file
            br = new BufferedReader(new FileReader("app/src/main/assets/rawData.csv"));
            //app/src/main/assets

            //Create List for holding Employee objects
            List<Account> accList = new ArrayList<Account>();

            //Tree Map
            TreeMap<String, String> tree_map = new TreeMap<String, String>();

            String line = "";
            //Read to skip the header
            br.readLine();
            //Reading from the second line
            while ((line = br.readLine()) != null)
            {
                String[] accountDetails = line.split(COMMA_DELIMITER);

                if(accountDetails.length > 0 )
                {
                    //Save the employee details in Employee object
                    Account acc = new Account (accountDetails[0],
                            accountDetails[1],accountDetails[2],
                            accountDetails[3]);

                    //Put Object in Map
                    tree_map.put(accountDetails[0],accountDetails[1]);


                }

            }

            //Print Treemap
            System.out.println("Initial Mappings are: " + tree_map);


            //Lets print the Employee List
            /*for(Account a : accList)
            {
                System.out.println(a.getAccountNumber()+"   "+a.getAccountType()+"   "
                        + a.getName() + "   "+a.getFamilyName());
            }*/
        }
        catch(Exception ee)
        {
            ee.printStackTrace();
        }
        finally
        {
            try
            {
                br.close();
            }
            catch(IOException ie)
            {
                System.out.println("Error occured while closing the BufferedReader");
                ie.printStackTrace();
            }
        }

    }

}
