package com.example.th.collections;

public class Account {

    //Class Properties
    private String accountNumber;
    private String accountType;
    private String givenName;
    private String familyName;

    //constructor
    public Account(String accountNumber, String accountType, String givenName, String familyName){

        super();
        this.accountNumber = accountNumber;
        this.accountType = accountType;
        this.givenName = givenName;
        this.familyName = familyName;

    }

    //Getter


    public String getAccountNumber(){
        return accountNumber;

    }
    public String getAccountType(){
        return accountType;

    }
    public String getName(){
        return givenName;

    }
    public String getFamilyName(){
        return familyName;

    }


    //Setter

    public void setAccountNumber(String accountNumber){
        this.accountNumber = accountNumber;
    }

    public void setAccountType(String accountType){
        this.accountType = accountType;
    }

    public void setGivenName(String givenName){
        this.givenName = givenName;
    }

    public void setFamilyName(String familyName){
        this.familyName = familyName;
    }

    @Override
    public String toString(){
        return "Account Number: " + accountNumber + " Account Type: " + accountType + " Given Name: "
                + givenName + " Family Name: " + familyName;
    }


    //

}
